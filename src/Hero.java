/**
 * Created by tiami on 03/08/2017.
 */
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.*;

public class Hero extends JFrame {
    private static JButton shaman = new JButton("Shaman");
        private static JButton druid = new JButton("Druid");
        private static JButton hunter = new JButton("Hunter");
        private static JButton mage = new JButton("Mage");
        private static JButton paladin = new JButton("Paladin");
        private static JButton priest = new JButton("Priest");
        private static JButton rogue = new JButton("Rogue");
        private static JButton warlock = new JButton("Warlock");
        private static JButton warrior = new JButton("Warrior");

        public static void main(String[] args) throws IOException {

            JFrame frame = new JFrame();

            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

            //make shaman
            shaman.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("shaman");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make druid
            druid.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("druid");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make hunter
            hunter.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("hunter");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make mage
            mage.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("mage");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make paladin
            paladin.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("paladin");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make priest
            priest.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("priest");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make rogue
            rogue.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("rogue");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make warlock
            warlock.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("warlock");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //make warrior
            warrior.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Reader.createHero("warrior");
                    Gui.makeGui();
                    frame.dispose();
                }
            });

            //"choose your hero" label
            JLabel text = new JLabel();
            text.setPreferredSize(new Dimension(230,30));
            text.setText("Choose your hero: ");
            Font myFont = new Font("Georgia", Font.BOLD, 20);
            text.setForeground(new Color(0,0,0));
            text.setFont(myFont);

            //text area
            JTextPane textArea = new JTextPane();
            textArea.setContentType("text/html");
            JScrollPane scrollPane = new JScrollPane(textArea);
            scrollPane.setBackground(new Color(176,135,91));
            textArea.setBackground(new Color(176,135,91));
            scrollPane.setPreferredSize(new Dimension(460,100));
            textArea.setEditable(false);
            StringBuilder sb = new StringBuilder();

            //Append text to string-builder for text area
            sb.append("<b>Current hero tier list: <br>");

            //Fetch current tier list from Icy Veins, and append to string-builder for text area
            String url = "https://www.icy-veins.com/hearthstone/arena-guide";
            Document doc = Jsoup.connect(url).get();
            Elements paragraphs = doc.select("ol");
            for(Element p: paragraphs) {
                sb.append(p.toString());
            }

            //append to string-builder for text area
            sb.append("</b>");
            //set text area to contain the string-builder
            textArea.setText(sb.toString());

            //create frame
            frame.getContentPane().setBackground(new Color(97,56,11));
            frame.setPreferredSize(new Dimension(500, 240));
            frame.setLayout(new FlowLayout(FlowLayout.LEFT));
            frame.add(scrollPane);
            frame.add(text);
            frame.add(shaman);
            frame.add(druid);
            frame.add(hunter);
            frame.add(mage);
            frame.add(paladin);
            frame.add(priest);
            frame.add(rogue);
            frame.add(warlock);
            frame.add(warrior);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }

    }
