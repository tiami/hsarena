/**
 * Created by tiami on 03/08/2017.
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Gui extends JFrame {
    private static JTextField card1 = new JTextField(15);
    private static JTextField card2 = new JTextField(15);
    private static JTextField card3 = new JTextField(15);
    private static JTextPane result = new JTextPane();
    private static JButton go = new JButton("Compare");

    public static void makeGui() {

        JFrame frame = new JFrame();

        //create space for prettiness
        JLabel space = new JLabel();
        space.setText("");
        space.setPreferredSize(new Dimension(700,10));

        //create text labels for card 1, 2 and 3
        JLabel text1 = new JLabel();
        text1.setText("Card 1: ");
        text1.setForeground(new Color(0,0,0));

        JLabel text2 = new JLabel();
        text2.setText("Card 2: ");
        text2.setForeground(new Color(0,0,0));

        JLabel text3 = new JLabel();
        text3.setText("Card 3: ");
        text3.setForeground(new Color(0,0,0));


        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //create compare button
        go.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result.setText(Search.compare(card1.getText(),card2.getText(),card3.getText()));
            }
        });

        //create result field
        result.setContentType("text/html");
        result.setBackground(new Color(176,135,91));
        result.setEditable(false);
        result.setPreferredSize(new Dimension(460,100));

        //create frame
        frame.setPreferredSize(new Dimension(705, 200));
        frame.getContentPane().setBackground(new Color(97,56,11));
        frame.setLayout(new FlowLayout(FlowLayout.LEFT));
        frame.add(text1);
        frame.add(card1);
        frame.add(text2);
        frame.add(card2);
        frame.add(text3);
        frame.add(card3);
        frame.add(space);
        frame.add(go);
        frame.add(result);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}