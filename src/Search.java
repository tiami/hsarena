import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by tiami on 03/08/2017.
 */
public class Search {
    public static  String compare(String card1, String card2, String card3){

        int c1 = -1;
        int c2 = -1;
        int c3 = -1;

        String tier1 = "<b>NOT FOUND</b>. Check if you spelled correctly!";
        String tier2 = "<b>NOT FOUND</b>. Check if you spelled correctly!";
        String tier3 = "<b>NOT FOUND</b>. Check if you spelled correctly!";

        //get hm fra Reader.
        HashMap<Integer,ArrayList<String>> hm = Reader.getHm();

        //return the tier of each card
        int size = hm.size();
        for(int i=0; i<size; i++){
            ArrayList<String> current = hm.get(i+1);
            if(current.contains(card1)){
                c1 = i+1;
            }
            if(current.contains(card2)){
                c2 = i+1;
            }
            if(current.contains(card3)){
                c3 = i+1;
            }
        }

       //DEBUG: System.out.println(c1 + " " + c2 + " " + c3);
        if(c1>0){
            tier1 = findTier(c1);
        }
        if(c2>0){
            tier2 = findTier(c2);
        }
        if(c3>0){
            tier3 = findTier(c3);
        }

        //make a html String result with each card listed, with their tier, or return card (1,2 eller 3) not found.
        String result = "<b>Arena ranking of the three cards: </b><br><table><tr><td>" + card1 + "</td><td>" + tier1 + "</td></tr>" +
                "<tr><td>" + card2 + "</td><td>" + tier2 + "</td></tr><tr><td>" + card3 + "</td><td>" + tier3 + "</td></tr></table>";

        //DEBUG: System.out.println(result);
        return result;
    }

    private static String findTier(int i){

        String result = "CARD NOT FOUND";

        if(i==1||i==9||i==17||i==25){
            result = "Tier 1: Excellent";
        }
        if(i==2||i==10||i==18||i==26){
            result = "Tier 2: Great";
        }
        if(i==3||i==11||i==19||i==27){
            result = "Tier 3: Good";
        }
        if(i==4||i==12||i==20||i==28){
            result = "Tier 4: Above Average";
        }
        if(i==5||i==13||i==21||i==29){
            result = "Tier 5: Average";
        }
        if(i==6||i==14||i==22||i==30){
            result = "Tier 6: Below Average";
        }
        if(i==7||i==15||i==23||i==31){
            result = "Tier 7: Bad";
        }
        if(i==8||i==16||i==24||i==32){
            result = "Tier 8: Terrible";
        }

        return result;
    }
}
