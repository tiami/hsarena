/**
 * Created by tiami on 03/08/2017.
 */

import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.*;

public class Reader {

    private static HashMap<Integer,ArrayList<String>> hm;

    public static void createHero(String s) {
        try {
            ArrayList<String> text = new ArrayList<>();
            int tier = 0;
            hm  = new HashMap<>();


            //creating the class-specific url by using class name (s)
            String url = "https://www.icy-veins.com/hearthstone/arena-" + s + "-tier-lists-journey-to-un-goro";

            //Getting all elements inside "th" and "td" html tags from above url. Adding each entry to the ArrayList "text".
            Document doc = Jsoup.connect(url).get();
            Elements paragraphs = doc.select("th, td");
            for(Element p: paragraphs) {
                text.add(p.text());
            }

            //creating a hashMap, saving tier numbers with their associated ArrayList of all card-names in that tier.
            for(String t:text){
                if(t.contains("Tier ")){
                    tier += 1;
                }
                else{
                    if(hm.containsKey(tier)){
                        ArrayList<String> current = hm.get(tier);
                        current.add(t);
                        hm.put(tier,current);
                    } else {
                        ArrayList<String> current = new ArrayList<>();
                        current.add(t);
                        hm.put(tier,current);
                    }
                }
            }

            //DEBUG: System.out.println(hm.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static HashMap<Integer,ArrayList<String>> getHm(){
        return hm;
    }
}